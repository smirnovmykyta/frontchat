import React, { useEffect } from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import { Box, makeStyles, Toolbar } from '@material-ui/core';
import { useSessionStorage } from 'react-use';

import { selectUserAuthData } from '../Auth/selectors';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  list: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

export const People = () => {
  const classes = useStyles();
  const userAuthData = useSelector(selectUserAuthData, shallowEqual);
  const [value, setValue] = useSessionStorage('userAuthData');

  useEffect(() => {
    if (!value) {
      setValue(userAuthData);
    }
  }, []);

  return (
    <Box>
      <Toolbar />
      <h1>People</h1>
    </Box>
  );
};
