import { put, takeLatest, call, select, take, fork } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';

import { WS_BASE } from '../../config';
import { selectUserAuthData } from '../Auth/selectors';
import {
  APP_START_INIT,
  INIT_SOCKET,
  SOCKET_CONNECT,
  SOCKET_MESSAGE_SEND,
  SOCKET_CLOSE,
  SOCKET_OPEN,
  SOCKET_MESSAGE_RECEIVE,
  USER_GET_SUCCESS,
  USERS_GET_SUCCESS,
  ROOMS_GET_SUCCESS,
} from './constants';
import {
  actionUserGetSuccess,
  actionUsersGetSuccess,
  actionRoomsGetSuccess,
  actionAppFinishInit,
  actionInitSocket,
  actionSocketConnect,
  actionSocketOpen,
  actionSocketClose,
  actionSocketError,
  actionSocketMessageReceive,
  actionSocketMessageSend,
} from './actions';

function* handleAppStartInit() {
  const { tokens } = yield select(selectUserAuthData);

  yield put(actionInitSocket({ token: tokens.accessToken }));
}

function* handleSocketInit(action) {
  const { payload } = action;

  const wsUrl = `${WS_BASE}?token=${payload.token}`;
  try {
    const socket = new WebSocket(wsUrl);
    yield put(actionSocketConnect({ socket }));
  } catch (e) {
    console.error(e);
  }
}

function watchMessage(socket) {
  return eventChannel((emitter) => {
    socket.onopen = (event) => emitter(actionSocketOpen(event));
    socket.onclose = (event) => emitter(actionSocketClose(event));
    socket.onerror = (event) => emitter(actionSocketError(event));
    socket.onmessage = (event) => emitter(actionSocketMessageReceive(JSON.parse(event.data)));

    return () => {
      socket.close();
    };
  });
}

function* internalListener(socket) {
  while (true) {
    const action = yield take(SOCKET_MESSAGE_SEND);
    socket.send(JSON.stringify(action.payload));
  }
}

function* externalListener(socketChannel) {
  while (true) {
    const action = yield take(socketChannel);
    yield put(action);
  }
}

function* handleSocketManager(action) {
  const { payload } = action;

  const socketChannel = yield call(watchMessage, payload.socket);

  yield fork(externalListener, socketChannel);
  yield fork(internalListener, payload.socket);

  const cancel = yield take(SOCKET_CLOSE);

  if (cancel) {
    socketChannel.close();
  }
}

function* handleGetInitialData() {
  const { userId } = yield select(selectUserAuthData);

  yield put(actionSocketMessageSend({ command: 'getCurrentUser', data: { userId } }));
  yield put(actionSocketMessageSend({ command: 'getAllUsers' }));
  yield put(actionSocketMessageSend({ command: 'getAllRooms' }));

  yield take([USER_GET_SUCCESS, USERS_GET_SUCCESS, ROOMS_GET_SUCCESS]);
  yield put(actionAppFinishInit());
}

function* receiveMessages(action) {
  const { payload } = action;

  switch (payload.command) {
    case 'currentUser': {
      yield put(actionUserGetSuccess({ currentUser: payload.currentUser }));
      break;
    }
    case 'users': {
      yield put(actionUsersGetSuccess({ users: payload.users }));
      break;
    }
    case 'rooms': {
      yield put(actionRoomsGetSuccess({ rooms: payload.rooms }));
      break;
    }
  }
}

export function* appSaga() {
  yield takeLatest(APP_START_INIT, handleAppStartInit);
  yield takeLatest(INIT_SOCKET, handleSocketInit);
  yield takeLatest(SOCKET_CONNECT, handleSocketManager);
  yield takeLatest(SOCKET_OPEN, handleGetInitialData);
  yield takeLatest(SOCKET_MESSAGE_RECEIVE, receiveMessages);
}
