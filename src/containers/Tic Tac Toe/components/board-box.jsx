import React from 'react';

// Create Box component
export const Box = (props) => {
  return (
    // eslint-disable-next-line react/destructuring-assignment
    <button className="board__box" onClick={props.onClick}>
      {/* eslint-disable-next-line react/destructuring-assignment */}
      {props.value}
    </button>
  );
};
